package com.futuros.moveapps.servicio.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.futuros.moveapps.excepcion.ConsumoServicioException;
import com.futuros.moveapps.excepcion.ErrorDto;
import com.google.common.io.CharStreams;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;

public class GeneralDecoderImpl implements ErrorDecoder {


    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Exception decode(String methodKey, Response response) {

        Reader reader = null;

        ErrorDto error = new ErrorDto();

        try {
            reader = response.body().asReader();
            //Easy way to read the stream and get a String object
            String result = CharStreams.toString(reader);
            //use a Jackson ObjectMapper to convert the Json String into a
            //Pojo
            ObjectMapper mapper = new ObjectMapper();
            //just in case you missed an attribute in the Pojo
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            //init the Pojo
            error = mapper.readValue(result, ErrorDto.class);


        } catch (IOException e) {

            logger.error("error", e);
        }

        logger.error("respose {}", response);
        logger.error("error {}", error);

        return new ConsumoServicioException(response.status(), error.getCodigo(), error.getMensaje());
    }

}

