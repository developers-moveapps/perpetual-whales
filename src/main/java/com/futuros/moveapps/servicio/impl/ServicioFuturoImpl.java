package com.futuros.moveapps.servicio.impl;


import com.futuros.moveapps.config.Config;
import com.futuros.moveapps.config.ConfigErrores;
import com.futuros.moveapps.dto.Futuros;
import com.futuros.moveapps.dto.OtherPositionRetList;
import com.futuros.moveapps.dto.RequestFuturosBinance;
import com.futuros.moveapps.dto.RespuestaFuturos;
import com.futuros.moveapps.entidad.EntityFuturos;
import com.futuros.moveapps.repositorio.RepositorioFuturos;
import com.futuros.moveapps.servicio.IFeign.ServicioBuscarFuturoBinance;
import com.futuros.moveapps.servicio.IServicioFuturos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioFuturoImpl implements IServicioFuturos {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ConfigErrores configErrores;

    @Autowired
    private Config config;

    @Autowired
    private ServicioImplHelper servicioImplHelper;

    @Autowired
    private RepositorioFuturos repositorioFuturos;

    @Autowired
    private ServicioBuscarFuturoBinance servicioBuscarFuturoBinance;

   /* @Override
    public RespuestaListarExamen buscar(UsuarioToken usuario, String texto) {

        List<EntityExamen> all = repositorioExamen.findByNombreContainsOrCodigoContains(texto, texto)
                .orElseThrow(IllegalArgumentException::new);

        logger.info(DataLog.getMensaje(DataLog.
                getInstancia(configErrores.getListaErrores().get("codigoExito").getId(),
                        "", usuario.getUsername(), config.getIdService(),
                        "Examen listados con exito: " + all.size())));

        return RespuestaListarExamen.builder()
                .respuesta(servicioImplHelper.getExamenes(all))
                .codigo(configErrores.getListaErrores().get("codigoExito").getId())
                .mensaje(configErrores.getListaErrores().get("codigoExito").getMessage())
                .build();
    }*/

    @Override
    public RespuestaFuturos listar() throws IllegalAccessException {
/*
        List<EntityFuturos> all = repositorioExamen.findAll();

        logger.info(DataLog.getMensaje(DataLog.
                getInstancia(configErrores.getListaErrores().get("codigoExito").getId(),
                        "", usuario.getUsername(), config.getIdService(),
                        "Examenes listados con exito: " + all.size())));*/


        Futuros futuros = servicioBuscarFuturoBinance.buscar(RequestFuturosBinance.builder()
                .encryptedUid("CA7AC7A51569D13B0F57A875521972F3")
                .tradeType("PERPETUAL")
                .build());


        if (!futuros.getCode().equalsIgnoreCase("000000")) {
            throw new IllegalAccessException();
        }

        if (!futuros.getData().getOtherPositionRetList().isEmpty()) {
            corregirZonaHoraria(futuros);
        } else {
            //Si la lista no tiene posiciones cerrar todas las posiciones en la BD
            Long posicionesActivas = repositorioFuturos.findByActivoTrue();
            if (posicionesActivas > 0) {
                repositorioFuturos.updateEstadosActivos();
            }
            //TODO responder al bot cerrar todas las posiciones
        }

        verificarCambiosEnPosiciones(futuros);


        return RespuestaFuturos.builder()
                //.respuesta(servicioImplHelper.getfuturos(all))
                .respuesta(new Futuros())
                .codigo(configErrores.getListaErrores().get("codigoExito").getId())
                .mensaje(configErrores.getListaErrores().get("codigoExito").getMessage())
                .build();

    }

    private void verificarCambiosEnPosiciones(Futuros futuros) {
        List<EntityFuturos> posicionesActivas = repositorioFuturos.findByActivo(true);

        for (EntityFuturos entity : posicionesActivas) {
            for (OtherPositionRetList other : futuros.getData().getOtherPositionRetList()) {
                if (other.getSymbol().equalsIgnoreCase(entity.getMoneda())) {
                    //SI EL MONTO EN BD ES DISTINTO DEL SERVICIO LA POSICION ANTERIOR ES FALSA Y SE GUARDA NUEVA
                    if (other.getAmount().compareTo(entity.getMonto()) != 0) {
                        entity.setActivo(false);

                        repositorioFuturos.saveAndFlush(entity);
                    }
                }
            }
        }

    }

    private void corregirZonaHoraria(Futuros futuros) {
        futuros.getData().setUpdateTime(futuros.getData().getUpdateTime().minusHours(3));
        for (OtherPositionRetList other : futuros.getData().getOtherPositionRetList()) {
            other.setUpdateTime(other.getUpdateTime().minusHours(3));
        }
    }
}