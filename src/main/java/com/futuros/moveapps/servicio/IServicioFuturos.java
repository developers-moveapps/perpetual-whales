package com.futuros.moveapps.servicio;


import com.futuros.moveapps.dto.RespuestaFuturos;

public interface IServicioFuturos {

    RespuestaFuturos listar() throws IllegalAccessException;
}
