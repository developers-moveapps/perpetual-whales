package com.futuros.moveapps.servicio.IFeign;

import com.futuros.moveapps.dto.Futuros;
import com.futuros.moveapps.dto.RequestFuturosBinance;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "binance",
        url = "https://www.binance.com")
public interface ServicioBuscarFuturoBinance {

    @PostMapping(path = "/bapi/futures/v1/public/future/leaderboard/getOtherPosition",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    Futuros buscar(@RequestBody RequestFuturosBinance requestFuturosBinance);

}
