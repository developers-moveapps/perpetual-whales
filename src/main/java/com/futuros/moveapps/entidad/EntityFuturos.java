package com.futuros.moveapps.entidad;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "futuros")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EntityFuturos {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "futuros_id_seq")
    @SequenceGenerator(name = "futuros_id_seq", sequenceName = "futuros_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "usuario", unique = true)
    private String usuario;

    @Column(name = "moneda")
    private String moneda;

    @Column(name = "monto")
    private Double monto;

    @Column(name = "fecha")
    private LocalDateTime fecha;

    @Column(name = "activo")
    private Boolean activo;


}
