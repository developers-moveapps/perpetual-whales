package com.futuros.moveapps.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OtherPositionRetList implements Serializable {

    public String symbol;
    public Double entryPrice;
    public Double markPrice;
    public Double pnl;
    public Double roe;
    public LocalDateTime updateTime;
    public Double amount;
    public Long updateTimeStamp;
    public Boolean yellow;
    public Boolean tradeBefore;
}
