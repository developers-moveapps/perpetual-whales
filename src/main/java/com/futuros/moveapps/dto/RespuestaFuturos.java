package com.futuros.moveapps.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@ToString(callSuper = true)
@Data
@NoArgsConstructor
public class RespuestaFuturos extends Respuesta implements Serializable {

    private Futuros respuesta;

}
