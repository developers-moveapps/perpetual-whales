package com.futuros.moveapps.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@SuperBuilder
@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class Data implements Serializable {

        public List<OtherPositionRetList> otherPositionRetList;
        public LocalDateTime updateTime;
        public long updateTimeStamp;

}
