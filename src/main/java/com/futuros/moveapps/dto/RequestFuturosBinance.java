package com.futuros.moveapps.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestFuturosBinance implements Serializable {

    private String encryptedUid;
    private String tradeType;

}
