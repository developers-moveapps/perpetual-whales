package com.futuros.moveapps.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Futuros implements Serializable {

    public String code;
    public com.futuros.moveapps.dto.Data data;
    public Boolean success;
}
