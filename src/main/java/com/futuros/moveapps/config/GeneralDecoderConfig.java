package com.futuros.moveapps.config;

import com.futuros.moveapps.servicio.impl.GeneralDecoderImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.Collections;

@Configuration
public class GeneralDecoderConfig {


    @Bean
    MappingJackson2HttpMessageConverter feignFormEncoder() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));

        return converter;
    }

    @Bean
    GeneralDecoderImpl decoderTest() {
        return new GeneralDecoderImpl();
    }
}
