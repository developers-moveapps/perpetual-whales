package com.futuros.moveapps.config;

public class MensajeError {
    private String id;
    private String message;

    public MensajeError() {

    }

    public MensajeError(String id, String message) {
        setId(id);
        setMessage(message);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
