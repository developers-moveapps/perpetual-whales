package com.futuros.moveapps.config;

public class DataLog {

    private String idServicio;
    private String mensaje;
    private String codigo;
    private String user;
    private String trx;


    public DataLog(String codigo, String trx, String user, String idServicio, String mensaje) {
        this.idServicio = idServicio;
        this.mensaje = mensaje;
        this.codigo = codigo;
        this.user = user;
        this.trx = trx;
    }

    public String getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(String idServicio) {
        this.idServicio = idServicio;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTrx() {
        return trx;
    }

    public void setTrx(String trx) {
        this.trx = trx;
    }

    @Override
    public String toString() {
        String trxLog = "", userLog = "";
        if (this.trx != null && this.user != null) {
            trxLog = this.trx;
            userLog = this.user;
        }
        return "CODIGO=" + codigo + ";TRX=" + trxLog + ";IDSERVICIO=" + idServicio + ";USER=" + userLog + ";MENSAJE=" + mensaje;
    }

    public static DataLog getInstancia(String codigo, String trx, String user, String idServicio, String mensaje) {
        return new DataLog(codigo, trx, user, idServicio, mensaje);
    }

    public static String getMensaje(DataLog dataLog) {
        return dataLog.toString();
    }
}


