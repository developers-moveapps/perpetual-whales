package com.futuros.moveapps.excepcion;

import com.futuros.moveapps.config.Config;
import com.futuros.moveapps.config.ConfigErrores;
import com.futuros.moveapps.config.DataLog;
import com.futuros.moveapps.dto.Respuesta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class ControladorGeneralDeExcepciones {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Config config;

    @Autowired
    private ConfigErrores configErrores;

    public ControladorGeneralDeExcepciones() {
        super();
    }

    @ExceptionHandler(value = Throwable.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public Respuesta procesarErrorNoControlado(Exception ex) {

        escribirLog(configErrores.getListaErrores().get("codigoExito").getId(), "", "", config.getIdService(),
                "Ocurrio un error", ex);

        return new Respuesta(configErrores.getListaErrores().get("comisionCrudErrorGeneral").getId(),
                configErrores.getListaErrores().get("comisionCrudErrorGeneral").getMessage());
    }


    @ExceptionHandler(value = ConsumoServicioException.class)
    public ResponseEntity captchaExcepcion(Exception ex) {

        ConsumoServicioException cse = (ConsumoServicioException) ex;

        escribirLog(cse.getCodigo(), "", "", config.getIdService(),
                "Status: " + cse.getStatus() + " " + cse.getMensaje());

        Respuesta respuesta = Respuesta.builder()
                .codigo(cse.getCodigo())
                .mensaje(cse.getMensaje())
                .build();

        return new ResponseEntity<Respuesta>(respuesta, HttpStatus.valueOf(cse.getStatus()));
    }


    private void escribirLog(String codigo, String trx, String rut, String idServicio,
                             String mensaje) {

        DataLog dataLog = DataLog.
                getInstancia(codigo, trx, rut, idServicio, mensaje);

        logger.error(DataLog.getMensaje(dataLog));
    }

    private void escribirLog(String codigo, String trx, String rut,
                             String idServicio, String mensaje, Exception ex) {

        DataLog dataLog = DataLog.
                getInstancia(codigo, trx, rut, idServicio, mensaje);

        logger.error(DataLog.getMensaje(dataLog), ex);
    }

}
