package com.futuros.moveapps.excepcion;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ConsumoServicioException extends RuntimeException {


    private Integer status;
    private String codigo;
    private String mensaje;

    public ConsumoServicioException(Integer status, String codigo, String mensaje) {
        super();
        this.status = status;
        this.codigo = codigo;
        this.mensaje = mensaje;
    }


}
