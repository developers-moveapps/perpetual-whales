package com.futuros.moveapps.repositorio;

import com.futuros.moveapps.entidad.EntityFuturos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


public interface RepositorioFuturos extends JpaRepository<EntityFuturos, Long> {

    Long findByActivoTrue();

    @Transactional
    @Modifying
    @Query("UPDATE EntityFuturos SET activo = true WHERE activo = false")
    Integer updateEstadosActivos();

    EntityFuturos findTopByOrderByIdDesc();

    List<EntityFuturos> findByActivo(Boolean estado);

}
