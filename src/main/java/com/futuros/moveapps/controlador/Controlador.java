package com.futuros.moveapps.controlador;

import com.futuros.moveapps.config.Config;
import com.futuros.moveapps.config.ConfigErrores;
import com.futuros.moveapps.config.DataLog;
import com.futuros.moveapps.dto.RespuestaFuturos;
import com.futuros.moveapps.servicio.IServicioFuturos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IServicioFuturos iServicioFuturos;

    @Autowired
    private Config config;

    @Autowired
    private ConfigErrores configErrores;

    @GetMapping(path = "/listar",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public RespuestaFuturos consultar() {

        DataLog dataLog = DataLog.
                getInstancia(configErrores.getListaErrores().get("codigoExito").getId(), "", "",
                        config.getIdService(), "Listando");

        logger.info(DataLog.getMensaje(dataLog));

        return iServicioFuturos.listar();
    }


}
